#include <Adafruit_NeoPixel.h>

/* --- YOU CAN CHANGE THIS --- */
#define NUMPIXELS           18        //number of LEDs

//colors, you can change the hex value to your prefference (https://www.google.com/search?q=color+picker)
#define COLOR_BOTTOM        0xFF0000  //hex color RED
#define COLOR_MID           0x949400  //hex color YELLOW
#define COLOR_TOP           0x00F000  //hex color GREEN

//depending on how many LEDs you have,
//you may want to change where each color starts
#define COLOR_BOTTOM_START  0
#define COLOR_MID_START     6
#define COLOR_TOP_START     13

//uncomment following line to use fixed brightness, or leave it commented to use potentiometer for brightness adjustment
//#define BRIGHTNESS        50        //0 = off / 255 = max

#define BUZZER_REPEAT       5         //when reaching the last LED buzzer will turn on and off, how many times?
#define BUZZER_DELAY        50        //lower value -> higher pitch. Test to find what you like

//speed of adding and substracting lights to the meter
#define SUBSTRACT_DELAY     400       //when not pressing, speed of LED reduction (in milliseconds)
#define ADD_DELAY           300       //while pressing, speed of LED increment (in milliseconds)

//arduino Pins
#define POT                 A0        //potentiometer
#define RF_A                2         //A channel of the receiver
#define RF_B                3         //B channel of the receiver
#define RF_C                4         //C channel of the receiver
#define RF_D                5         //D channel of the receiver
#define BUZZER_PIN          9         //pin for the buzzer
#define PIXEL_PIN           10        //pin for the LED strip
/* --------------------------- */

/* --- DO NOT CHANGE --- */
#define COLOR_BOTTOM_END    (COLOR_MID_START - 1)
#define COLOR_MID_END       (COLOR_TOP_START - 1)
#define COLOR_TOP_END       (NUMPIXELS - 1)

#define CYCLE_DELAY           10
#define TOT_SUBSTRACT_CYCLES  (SUBSTRACT_DELAY / CYCLE_DELAY)
#define TOT_ADD_CYCLES        (ADD_DELAY / CYCLE_DELAY)

Adafruit_NeoPixel ledStrip = Adafruit_NeoPixel(NUMPIXELS, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

int currentLed = COLOR_BOTTOM_START;
int substractCounter = 0;
int addCounter = 0;
/* --------------------- */

/*
 * Setup of the program
 * Only runs once
 */
void setup(void) {
  //config pins
  pinMode(RF_A, INPUT);
  pinMode(RF_B, INPUT);
  pinMode(RF_C, INPUT);
  pinMode(RF_D, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  
  ledStrip.begin(); //this initializes the NeoPixel library.
}

/*
 * Main loop
 * Run continuosly
 */
void loop(void) {
  if((digitalRead(RF_A) == HIGH) || (digitalRead(RF_B) == HIGH) || (digitalRead(RF_C) == HIGH) || (digitalRead(RF_D) == HIGH)){
    if(addCounter++ >= TOT_ADD_CYCLES){
      addCounter = 0; //restart counter
      Add_One();      //turn on next LED from the strip
    }
  }
  //no button is being pressed
  else{
    if(substractCounter++ >= TOT_SUBSTRACT_CYCLES){
      substractCounter = 0; //restart counter
      Substract_One();      //turn off last LED from the strip
    }
  }
  
  delay(CYCLE_DELAY);
}

/*
 * Turn on next LED of the strip
 */
void Add_One(void){
  if(currentLed < NUMPIXELS){
    //if all LEDs are off we read the pot to adjust the brightness
    if(currentLed == COLOR_BOTTOM_START){
      Set_Brightness();
    }
    
    //LED is RED?
    if((currentLed >= COLOR_BOTTOM_START) && (currentLed <= COLOR_BOTTOM_END)){
      ledStrip.setPixelColor(currentLed, COLOR_BOTTOM);
      ledStrip.show();
    }
    //LED is YELLOW?
    else if((currentLed >= COLOR_MID_START) && (currentLed <= COLOR_MID_END)){
      ledStrip.setPixelColor(currentLed, COLOR_MID);
      ledStrip.show();
    }
    //LED is GREEN?
    else if((currentLed >= COLOR_TOP_START) && (currentLed <= COLOR_TOP_END)){
      ledStrip.setPixelColor(currentLed, COLOR_TOP);
      ledStrip.show();

      //if its the last LED from the strip we play activate the buzzer
      if(currentLed == COLOR_TOP_END){
        Play_Buzzer();  //activate the buzzer
      }
    }

    if(currentLed < COLOR_TOP_END){
      currentLed++;
    }
  }
}

/*
 * Turn off last LED of the strip
 */
void Substract_One(void){
  if(currentLed >= COLOR_BOTTOM_START){
    ledStrip.setPixelColor(currentLed, 0); //turn off current LED
    ledStrip.show();
    
    currentLed--;
  }
}

/*
 * Activate the buzzer
 */
void Play_Buzzer(void){
  for(byte x = 0; x < BUZZER_REPEAT; x++){
    digitalWrite(BUZZER_PIN, HIGH);
    delay(BUZZER_DELAY);
    digitalWrite(BUZZER_PIN, LOW);
    delay(BUZZER_DELAY);
  }
}

/*
 * Set brightness of the LEDs
 * Choose lowes brightness possible to save on batteries
 * 
 * If BRIGHTNESS is defined, then use fixed brightness defined
 * If not, read the potentiometer to adjust brightness
 * Analog pin returns a value from 0 to 1023
 * We convert it to a value from 1 to 255 needed for brightness control
 */
void Set_Brightness(void){
#ifdef BRIGHTNESS
  ledStrip.setBrightness(BRIGHTNESS); //use fixed brightness
#else
  byte potValue = map(analogRead(POT), 0, 1023, 255, 1);  //read POT, and convert value
  ledStrip.setBrightness(potValue); //set strip brightness
#endif
}
